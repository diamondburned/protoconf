package protoconf

type Driver interface {
	Marshal(v interface{}) ([]byte, error)
	Unmarshal(data []byte, v interface{}) error
}

// DriverWithMapper uses the Driver's map implementation instead of the built-in
// unordered map. An advantage of this would be ordered maps, but it is much
// harder to implement.
//
// This interface should not be thread-safe. It should also not be re-usable.
type DriverWithMapper interface {
	Driver
	Mapper
	// This is really the only function that needs to be manually implemented
	MarshalMap() ([]byte, error)
}
