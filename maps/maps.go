package maps

import (
	"gitlab.com/diamondburned/protoconf"
)

// Map implements a thread-unsafe ordered key-value pair.
type Map struct {
	Keys   []string
	Values map[string]interface{}
}

var _ protoconf.Mapper = (*Map)(nil)

func New() *Map {
	return &Map{
		Values: map[string]interface{}{},
	}
}

func (m *Map) ClearMap(newCap int) {
	if len(m.Keys) > newCap {
		m.Keys = m.Keys[:0]
	} else {
		m.Keys = make([]string, 0, newCap)
	}

	m.Values = make(map[string]interface{}, newCap)
	return
}

func (m *Map) SetMap(k string, v interface{}) {
	m.Keys = append(m.Keys, k)
	m.Values[k] = v
}

func (m *Map) NewMap(k string) protoconf.Mapper {
	m.Keys = append(m.Keys, k)
	n := New()
	m.Values[k] = n
	return n
}

func (m *Map) GetMap() map[string]interface{} {
	return m.Values
}
