package protoconf

import (
	"errors"
	"reflect"
	"strings"
)

var ErrUnknownType = errors.New("Unknown type, possibly not a struct/ptr to struct")
var ErrMismatchStruct = errors.New("Struct type is different")

type Defaulter interface {
	Default() Defaulter
}

type Struct struct {
	opts Options

	mapper    Mapper
	mapperDri DriverWithMapper
	driver    Driver

	nameMap    map[string]string
	structType reflect.Type

	// default, not used rn
	// def interface{}
}

type Options struct {
	// When true, initializes all nil structs to its non-pointer zero-value
	InitZeroValue bool

	// Default is CamelCase to snake_case
	CaseConverter func(string) string
}

// New creates a new Struct. Driver is not optional. If value is nil but has a
// type, its default will be used. Check the Defaulter interface.
func New(value interface{}, d Driver, opts *Options) (*Struct, error) {
	if opts == nil {
		opts = &Options{
			InitZeroValue: true,
			CaseConverter: CamelToSnake,
		}
	}

	var s = Struct{
		opts:    *opts,
		driver:  d,
		nameMap: map[string]string{},
	}

	if d, ok := d.(DriverWithMapper); ok {
		// If the driver does implement its own Map, we'll use that
		s.mapper = d
		s.mapperDri = d
	} else {
		// If the driver doesn't implement a Map, we use a fallback, non-ordered
		// normal map.
		s.mapper = FallbackMap{}
	}

	// See if value implements a method to get the Default values
	if d, ok := value.(Defaulter); ok {
		// it does, fill this up, future use?
		// s.def = d

		// definitely use this if the passed in value is a nil value but has a
		// concrete type
		if reflect.ValueOf(value).IsNil() {
			value = d.Default()
		}
	}

	v := reflect.ValueOf(value)
	t := v.Type()

	// Check if value is a struct/ptr to struct, error out if not
	switch t.Kind() {
	case reflect.Struct:
		// noop, continue
	case reflect.Ptr:
		// dereference the type
		t := v.Type().Elem()
		// if underlying type is not a struct
		if t.Kind() != reflect.Struct {
			return nil, ErrUnknownType
		}
	default:
		// definitely not a struct
		return nil, ErrUnknownType
	}

	// save type for future comparison
	s.structType = t

	// start filling up the map
	s.reflectToMap(v, t, s.mapper)

	return &s, nil
}

func (s *Struct) Marshal() ([]byte, error) {
	if s.mapperDri != nil { // if driver implements a custom mapper
		return s.mapperDri.MarshalMap()
	}

	return s.driver.Marshal(s.mapper)
}

// into must be the right destination struct, else reflect would panic
func (s *Struct) Unmarshal(data []byte, into interface{}) error {
	m := make(map[string]interface{}, len(s.nameMap))
	if err := s.driver.Unmarshal(data, &m); err != nil {
		return err
	}

	return s.reflectFromMap(m, into)
}

func CamelToSnake(camel string) string {
	var snake strings.Builder
	snake.Grow(len(camel) * 2)

	for i := 0; i < len(camel); i++ {
		// is upper case
		if 'A' <= camel[i] && camel[i] <= 'Z' {
			if i > 0 { // not the start of string
				snake.WriteByte('_')
			}
			snake.WriteByte(camel[i] + 'a' - 'A') // to lower case
		} else {
			snake.WriteByte(camel[i])
		}
	}

	return snake.String()
}
