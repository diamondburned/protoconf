package json

import (
	"bytes"
	"encoding/json"

	"gitlab.com/diamondburned/protoconf/maps"
)

type Driver struct {
	// Inherits Map
	*maps.Map
}

func NewDriver() *Driver {
	return &Driver{
		Map: maps.New(),
	}
}

func (d *Driver) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (d *Driver) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (d *Driver) MarshalMap() ([]byte, error) {
	return d.marshalMap(d.Map)
}

func (d *Driver) marshalMap(m *maps.Map) ([]byte, error) {
	var byteSlice = make([][]byte, 0, len(m.Keys))

	for _, k := range m.Keys {
		// interface value pointer
		v := m.Values[k]
		// json value
		var jv []byte
		var err error

		// marshal key
		kb, err := json.Marshal(k)
		if err != nil {
			return nil, err
		}

		// if value is a nested map
		if m, ok := v.(*maps.Map); ok {
			// recursively marshal map
			jv, err = d.marshalMap(m)
		} else {
			// marshal value normally
			jv, err = json.Marshal(v)
		}

		if err != nil {
			return nil, err
		}

		byteSlice = append(byteSlice, append(append(kb, ':'), jv...))
	}

	// TODO: optimize
	return append(append([]byte{'{'}, bytes.Join(byteSlice, []byte{','})...), '}'), nil
}
