package json

import (
	"bytes"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/diamondburned/protoconf"
	"gitlab.com/diamondburned/protoconf/tests"
)

const result = `{"string":"default string","code":123,"camel_case":"key will be converted to snake_case with the magic of regex","property":{"mood":6969,"nest":{"thing":""}},"property2":{"mood":34,"nest":{"thing":""}},"empty_map":null,"proper_map":{"key":"value"}}`

// result with "string" altered
const resultAltered = `{"string":"not a default string","code":123,"camel_case":"key will be converted to snake_case with the magic of regex","property":{"mood":6969,"nest":{"thing":""}},"property2":{"mood":34,"nest":{"thing":""}},"empty_map":null,"proper_map":{"key":"value"}}`

func TestJSONEncode(t *testing.T) {
	b, err := tests.DriverEncode(NewDriver())
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(b, []byte(result)) {
		t.Fatal("Got a different result\n" + result)
	}
}

func TestJSONDecode(t *testing.T) {
	var c tests.Config

	s, err := protoconf.New(&c, NewDriver(), nil)
	if err != nil {
		t.Fatal(err)
	}

	if err := s.Unmarshal([]byte(resultAltered), &c); err != nil {
		t.Fatal(err)
	}

	if c.String != "not a default string" {
		spew.Dump(c)
		t.Fatal("Unexpected result for 'string'")
	}
}
