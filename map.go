package protoconf

type Mapper interface {
	// These 2 methods are implemented by maps.KV to save time.
	ClearMap(newCap int)
	GetMap() map[string]interface{}
	SetMap(k string, v interface{})
	NewMap(k string) Mapper
}

type FallbackMap map[string]interface{}

var _ Mapper = (FallbackMap)(nil)

func (m FallbackMap) ClearMap(newCap int) {
	m = make(FallbackMap, newCap)
	return
}

func (m FallbackMap) SetMap(k string, v interface{}) {
	m[k] = v
}

func (m FallbackMap) NewMap(k string) Mapper {
	n := FallbackMap{}
	m[k] = n
	return n
}

func (m FallbackMap) GetMap() map[string]interface{} {
	return m
}
