package tests

import (
	"gitlab.com/diamondburned/protoconf"
)

type Config struct {
	String    string
	Code      int
	CamelCase string
	Property  Property
	Property2 *Property
	EmptyMap  map[string]string
	ProperMap map[string]string
}

type Property struct {
	Mood uint
	Nest *Nest
}

type Nest struct {
	Thing *string
}

func (c *Config) Default() protoconf.Defaulter {
	return &Config{
		String:    "default string",
		Code:      123,
		CamelCase: "key will be converted to snake_case with the magic of regex",
		Property: Property{
			Mood: 6969,
		},
		Property2: &Property{
			Mood: 34,
		},
		ProperMap: map[string]string{
			"key": "value",
		},
	}
}

func DriverEncode(d protoconf.Driver) ([]byte, error) {
	var c *Config // expected to use the default

	s, err := protoconf.New(c, d, nil)
	if err != nil {
		return nil, err
	}

	return s.Marshal()
}
