package protoconf

import (
	"fmt"
	"reflect"

	"github.com/davecgh/go-spew/spew"
)

func (s *Struct) reflectToMap(v reflect.Value, t reflect.Type, mapper Mapper) interface{} {
	if t == nil {
		t = v.Type()
	}

	switch t.Kind() {
	case reflect.Struct:
		// noop, continue
	case reflect.Ptr:
		// dereference
		t = v.Type().Elem()
		v = v.Elem()
		if t.Kind() != reflect.Struct {
			// if not struct, return as is
			return v.Interface()
		}
	default:
		return v.Interface()
	}

	// Copy each field into the newly created map
	// TODO: change implementation
	for i, nf := 0, t.NumField(); i < nf; i++ {
		ftype := t.Field(i)
		fval := v.Field(i)
		if !fval.IsValid() {
			continue
		}

		// convert field name
		fname := s.opts.CaseConverter(ftype.Name)
		s.nameMap[ftype.Name] = fname // save for future use

		switch ftype.Type.Kind() {
		case reflect.Struct: // if field is a struct
			// recursively parse this struct
			s.reflectToMap(fval, nil, mapper.NewMap(fname))
		case reflect.Ptr: // if field is a pointer
			// if the pointer is uninitialized
			if fval.IsNil() {
				t := fval.Type()
				if s.opts.InitZeroValue {
					// convert the type to the underlying struct, if enabled
					t = t.Elem()
				}

				// create a new value from the type
				fval = reflect.New(t)
			}

			// check if type is a struct
			if t := fval.Type(); t.Elem().Kind() == reflect.Struct {
				// recursively parse this struct
				s.reflectToMap(fval, nil, mapper.NewMap(fname))
				continue // done
			}

			// if not a struct, parse it normally
			fallthrough
		default:
			mapper.SetMap(fname, fval.Interface())
		}
	}

	return nil
}

func (s *Struct) reflectFromMap(m map[string]interface{}, into interface{}) error {
	// convenience
	reverseNameMap := make(map[string]string, len(s.nameMap))
	for orig, mapped := range s.nameMap {
		reverseNameMap[mapped] = orig
	}

	// directly get the map since order doesn't matter
	// underlying := s.mapper.GetMap()

	v := reflect.ValueOf(into)
	t := v.Type()

	if t != s.structType {
		fmt.Println(t, s.structType)
		return ErrMismatchStruct
	}

	return s._reflectFromMap(m, into, reverseNameMap)
}

func (s *Struct) _reflectFromMap(m map[string]interface{}, into interface{}, reverseNameMap map[string]string) error {
	// directly get the map since order doesn't matter
	// underlying := s.mapper.GetMap()

	v := reflect.ValueOf(into)
	t := v.Type()

	if t.Kind() == reflect.Ptr {
		spew.Dump(m)
		v = reflect.Indirect(v)
		// t = v.Type()
	}

	fmt.Println(t)

	for mapped, value := range m {
		switch t.Kind() {
		case reflect.Map:
		default:
			// value of the destination struct
			dval := v.FieldByName(reverseNameMap[mapped])
			// value of the marshalled map
			fval := reflect.ValueOf(value)

			switch f := value.(type) {
			case float64:
				if dval.Type().Kind() == reflect.Int {
					dval.SetInt(int64(f))
				}
			case map[string]interface{}:
				i := dval.Interface()
				if err := s._reflectFromMap(f, i, reverseNameMap); err != nil {
					return err
				}
			default:
				dval.Set(fval)
			}
		}
	}

	return nil
}
